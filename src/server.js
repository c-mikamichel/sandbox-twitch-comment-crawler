const express = require('express');
const bodyParser = require('body-parser');
const exec = require('child_process').exec;

const port = 20203;
const app = express();

const CRAWLER_LAUNCH_COMMAND =
  'nohup node src/index.js crawl-comments __ID__ >>logs/crawling___ID__.log 2>&1 &';

function promisedExec(command) {
  return new Promise((resolve, reject) => {
    exec(command, (err, stdOut, stdError) => {
      if (err) {
        reject(err);
      }

      resolve({ stdOut, stdError });
    });
  });
}

// for parsing application/json
app.use(bodyParser.json());

app.all('/', (req, res) => {
  res.status(404).end();
});

// Make a response for the challenge request from Twitch.
app.get('/twitch-hook', async (req, res) => {
  console.info(
    `level:INFO\ttimestamp:${new Date().toISOString()}\tmessage:Challenge Request was received.`
  );
  res
    .status(200)
    .send(req.query['hub.challenge'])
    .end();
});

// Handling webhoook event.
app.post('/twitch-hook', async (req, res) => {
  console.info(
    `level:INFO\ttimestamp:${new Date().toISOString()}\tmessage:Request was received on POST:/twitch-hook.\tbody:${JSON.stringify(
      req.body
    )}`
  );

  const userIds = req.body.data.map(elm => elm.user_id);

  userIds.forEach(async userId => {
    console.info(
      `level:INFO\ttimestamp:${new Date().toISOString()}\tmessage:Launch ${userId}'s crawling.`
    );
    const execResult = await promisedExec(
      CRAWLER_LAUNCH_COMMAND.replace(/__ID__/g, userId)
    );
  });

  res.status(202).end();
});

app.listen(port);
