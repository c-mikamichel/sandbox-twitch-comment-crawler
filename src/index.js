const cac = require('cac');
const puppeteer = require('puppeteer');
const MongoClient = require('mongodb').MongoClient;
const crypto = require('crypto');
const axios = require('axios');

const cli = cac();

const mongoUrl = 'mongodb://localhost:27017';
const dbName = 'twitch-comment-analysing';

const commentAreaElementSelector =
  '#root > div > div.tw-full-width.tw-full-height.tw-flex > div > div > section > div > div.chat-list.tw-overflow-hidden.tw-flex.tw-flex-column.tw-flex-grow-1.tw-flex-nowrap > div.chat-list__lines.tw-flex-grow-1.scrollable-area > div.simplebar-scroll-content > div > div';

const TWITCH_CLIENT_ID = process.env.TWITCH_CLIENT_ID;

const axiosInstance = axios.create({
  baseURL: 'https://api.twitch.tv/kraken/',
  headers: {
    Accept: 'application/vnd.twitchtv.v5+json',
    'Client-ID': TWITCH_CLIENT_ID
  }
});

async function isStreaming(streamerName) {
  const info = await findStreamerInfoByName(streamerName);
  return isStreamingByUserId(info._id);
}

function isStreamingByUserId(userId) {
  return axiosInstance.get(`streams/${userId}`).then(response => {
    return response.data.stream !== null;
  });
}

function findStreamerInfoByName(name) {
  return axiosInstance
    .get('users', {
      params: {
        login: name
      }
    })
    .then(response => {
      if (response.data._total === 0) {
        throw new Error(`User(${name}) is not existing.`);
      }

      return response.data.users[0];
    });
}

/**
 *
 * @return streamerInfo
 * {
 *   "display_name": "DISPLAY_NAME",
 *   "_id": "12341234",
 *   "name": "name",
 *   "type": "user",
 *   "bio": "bio",
 *   "created_at": "2010-03-09T06:47:08.075757Z",
 *   "updated_at": "2018-05-06T10:05:06.837345Z",
 *   "logo": "https://static-cdn.jtvnw.net/jtv_user_pictures/1234123412341234-profile_image-300x300.jpeg"
 * }
 */
function findStreamerInfoById(id) {
  return axiosInstance.get(`users/${id}`).then(response => {
    if (response.data.error) {
      throw new Error(JSON.stringify(response.data));
    }

    return response.data;
  });
}

function establishConnection(url, dbName) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(
      url,
      (err, db) => {
        if (err) reject(err);

        const dbObject = db.db(dbName);

        resolve(dbObject);
      }
    );
  });
}

async function crawlComments(streamerId) {
  const info = await findStreamerInfoById(streamerId);

  // crawling開始前にstreamがonlineか確認
  // offlineであれば終了
  if (!(await isStreamingByUserId(streamerId))) {
    console.log(
      `${streamerName}'s stream is offline. This crawling task will be shutting donw.`
    );
    process.exit();
  }

  // init puppeteer
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(`https://www.twitch.tv/popout/${info.name}/chat`);

  // init mongo connection
  const db = await establishConnection(mongoUrl, dbName);

  // console.log('__mutation')が実行された際のハンドラ
  page.on('console', async msg => {
    if (msg._text !== '__mutation') {
      return;
    }

    const usernameElm = await page.$(
      `${commentAreaElementSelector} > div:last-child > button.chat-line__username > span > span`
    );
    const messageElm = await page.$(
      `${commentAreaElementSelector} > div:last-child`
    );

    // usernameElm及びmessageElmがnullの時はtwitchのシステムメッセージだったりなので除外
    // 何らかユーザコメント以外を収集対象に加えたい時はこの条件弄る必要あり？
    if (usernameElm === null || messageElm === null) {
      return;
    }

    const username = await (await usernameElm.getProperty(
      'innerHTML'
    )).jsonValue();

    // message(コメント本文)とその中で使用されているemotesを取り出す
    const messageChilds = await messageElm.$$('span');
    const emotes = [];
    let messageElms = await Promise.all(
      messageChilds.map(async elm => {
        const heightAdjaster = await elm.$('span.tw-placeholder');
        if (heightAdjaster) {
          return '';
        }

        // emoteの場合は::で囲んで返却
        const emote = await elm.$('img.chat-image');
        if (emote) {
          const emoteString = await (await emote.getProperty(
            'alt'
          )).jsonValue();
          emotes.push(emoteString);
          return `:${emoteString}:`;
        }

        return await (await elm.getProperty('innerHTML')).jsonValue();
      })
    );

    messageElms = messageElms.slice(messageElms.indexOf(': ') + 1);
    const message = messageElms.join('');

    // calculate username+message hash for objectId
    // mongo側でこの値にkeyを貼っておいて、コメントが重複して収集されないようにしたい
    const sha512 = crypto.createHash('sha512');
    sha512.update(`${username},${message}`);
    const recordHash = sha512.digest('hex');

    // create data object for mongo record.
    const data = {
      id: recordHash,
      username,
      badges: await Promise.all(
        (await messageElm.$$('img.chat-badge')).map(
          async elm => await (await elm.getProperty('alt')).jsonValue()
        )
      ),
      message,
      emotes,
      commented: new Date()
    };

    // mongoにドキュメントをinsert
    db.collection(`comments_${info.name}`).insertOne(data, (error, result) => {
      if (error) throw error;
    });
  });

  // evaluate()内はnode側でなくheadless chrome側で実行されるコード
  // MutationObserverを使用してコメント表示エリアの子elmに変更が入った場合にconsole.log('__mutation')を実行する様なコード
  await page.evaluate(() => {
    const observer = new MutationObserver(() => {
      console.log('__mutation');
    });
    const config = {
      childList: true
    };
    observer.observe(
      document.querySelector(
        '#root > div > div.tw-full-width.tw-full-height.tw-flex > div > div > section > div > div.chat-list.tw-overflow-hidden.tw-flex.tw-flex-column.tw-flex-grow-1.tw-flex-nowrap > div.chat-list__lines.tw-flex-grow-1.scrollable-area > div.simplebar-scroll-content > div > div'
      ),
      config
    );
  });

  // 10分毎にstreamがonline確認して、offlineであれば終了
  setInterval(async () => {
    if (!(await isStreaming(info.name))) {
      console.log(
        `${
          info.name
        }'s stream is offline. This crawling task will be shutting donw.`
      );
      await browser.close();
      process.exit();
    }
  }, 600000);
}

cli.command(
  'crawl-comments',
  {
    desc: '指定したStreamer IDの配信のコメントを収集する'
  },
  input => {
    crawlComments(input[0]);
  }
);

cli.parse();
