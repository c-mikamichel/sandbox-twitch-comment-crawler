## Twitch Comment Crawler

This is the application for crawling the contents of Twitch stream's comment.

The server(src/server.js) is waiting for the Twitch Webhook POST request which is informing the subscribed streamer's stream is upped.  
Then, the server is starting crawling process(src/index.js) for each stream.  
All crwaled comments are stored into MongoDB.

## How to use

```
npm run start-server
```

## refs.

[Twitch Webhooks Reference - Topic: Stream Up/Down](https://dev.twitch.tv/docs/api/webhooks-reference/#topic-stream-updown)  
[Run Puppeteer/Chrome Headless on EC2 Amazon Linux AMI](https://medium.com/mockingbot/run-puppeteer-chrome-headless-on-ec2-amazon-linux-ami-6c9c6a17bee6)

## LICENSE

MIT
